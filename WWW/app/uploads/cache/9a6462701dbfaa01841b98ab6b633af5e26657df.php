<footer class="content-info">

  <section class="instagram wow">
      <div class="container pb-100">
       
      <h3 class="title">Instagram</h3>
    </div>
    <div id="instafeed-gallery-feed" class="gallery row no-gutter">
  </section>

  <section class="p-100 pb-0 newsletter wow">
      <div class="container">
            

<!-- Begin Mailchimp Signup Form -->
        <form action="https://swim.us19.list-manage.com/subscribe/post?u=5063f24ee8e69f5b03bf3db7c&amp;id=fe14ede559" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline" target="_blank" novalidate>
          <div class="form-group d-fle flex-wrap justify-content-between mc-field-group">
                        <h3>Newsletter</h3>
                            <input type="email" name="EMAIL" class="form-control form-control-lg required email" value="" id="mce-EMAIL" aria-describedby="emailHelp" placeholder="E-mail Address">
                            <button type="submit" class="btn btn-submit mb-2" name="subscribe" id="mc-embedded-subscribe">Subscribe</button>
                          </div>


                          <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>                          </form>
<!--End mc_embed_signup-->

        </div>
        <div id="mce-responses" class="clear col-xl-10 mx-auto">
          <div class="response" id="mce-error-response" style="display:none"></div>
          <div class="response" id="mce-success-response" style="display:none"></div>
        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
      </section>


<section class="footer-bottom">
  <div class="p-100"></div>
    <div class="container text-left">
        <a class="brand" href="<?php echo e(home_url('/')); ?>"></a>
      </div>

      <div class="container pb-100">

        <div class="row justify-content-between">
            <div class="col-lg-7">
               <?php echo e(the_field ('contact', 'option')); ?>

              </div>
              <div class="col-lg-5">
                  <?php dynamic_sidebar('sidebar-footer') ?>
                  <div class="widget">
                    <div class="custom-html-widget">

                      
                      <a href=" <?php echo e(the_field ('instagram', 'option')); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                      <a href=" <?php echo e(the_field ('tripadvisor', 'option')); ?>" target="_blank"><i class="fa fa-tripadvisor"></i></a>
                      <a href="<?php echo e(the_field ('twitter', 'option')); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                      <a href="<?php echo e(the_field ('facebook', 'option')); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                      
                    
                    </div>
                  </div>
              </div>
        </div>

        </div>
</section>
</footer>
