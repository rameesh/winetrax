<?php 

$custom_query_args = array( 

            'post_type'      => 'product',
            'posts_per_page' => -1,
            'orderby'   => 'id',
            'order' => 'asc', // Randomize the results
            'operator' => 'IN',
            'post_status' => 'publish',
        
      );
        // Initiate the custom query
        $custom_query = new WP_Query( $custom_query_args );
        
        if ( $custom_query->have_posts() ):
        $price = get_post_meta( get_the_ID(), '_regular_price', true);
        ?>

<section class="<?php if(is_singular('product')): ?>more-packages <?php else: ?> p-100 pb-0 <?php endif; ?> tour-packages wow">
        <div class="container">
            <h3>Tour Packages</h3>
        </div>
        
        <div class="container d-flex pt-5 pb-100 flex-wrap" id="tour-packages">

            <?php 
            while ( $custom_query->have_posts() ) : $custom_query->the_post();
            
      ?>

            <!-- do stuff -->
          
          
                <div class="tour-box px-1 mb-5">
                        

                                <?php  do_action( 'woocommerce_shop_loop' );
                                wc_get_template_part( 'content', 'product' );?>
                                
                </div>
                <?php endwhile; ?>

  
      <?php (wp_reset_postdata()); ?> 
          </div>
        </section>
        <?php endif; ?>

        