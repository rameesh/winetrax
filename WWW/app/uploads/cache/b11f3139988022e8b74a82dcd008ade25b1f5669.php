
<section class="p-100 single-content wow" id="product-<?php the_ID(); ?>"> 
  <div class="container px-lg-0 woocommerce">
      <article <?php post_class() ?>>
          <div class="single-product">

            <?php 
          do_action( 'woocommerce_before_single_product' );
          if ( post_password_required() ) {
            echo get_the_password_form(); // WPCS: XSS ok.
            return;
          }
?>
            
          <div class="entry-content">

            <div class="summary entry-summary">
             <?php do_action( 'woocommerce_single_product_summary' )?>
            </div>

      <div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
        <?php do_action( 'woocommerce_before_single_product_summary' )?>

        </div>

        <div itemprop="description">
          <?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>
        

          </div>
        
          <footer>
            <?php echo wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

          </footer>
        </div>
        </article>
  </div>
</section>
<?php //comments_template('/partials/comments.blade.php') ?>