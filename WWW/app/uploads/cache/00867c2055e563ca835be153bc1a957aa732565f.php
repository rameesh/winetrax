

<?php if(!empty($breadcrumb)): ?>
    <nav class="sw-breadcrumbs" aria-label="breadcrumb">
        <ol class="breadcrumb">
			<?php foreach ( $breadcrumb as $key => $crumb ): ?>
            <?php if(! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1): ?>
                <li class="breadcrumb-item">
                    <a href="<?php echo e(esc_url( $crumb[1] )); ?>"><?php echo e(esc_html( $crumb[0] )); ?></a>
                </li>
            <?php else: ?>
                <li class="breadcrumb-item active" aria-current="page">
                    <?php echo e(esc_html( $crumb[0] )); ?>

                </li>
            <?php endif; ?>
			<?php endforeach; ?>
        </ol>
    </nav>
<?php endif; ?>
