<?php
    global $product
?>

<?php if(empty($product) || ! $product->is_visible()): ?>
    return
<?php endif; ?>

<div <?php echo e(post_class('sw-product-archive__product col-sm-12 col-md-6 col-lg-3')); ?>>
    <?php do_action( 'woocommerce_before_shop_loop_item' ) ?>
    <?php do_action( 'woocommerce_before_shop_loop_item_title' ) ?>
    <?php do_action( 'woocommerce_shop_loop_item_title' ) ?>
    <?php do_action( 'woocommerce_after_shop_loop_item_title' ) ?>
    <?php do_action( 'woocommerce_after_shop_loop_item' ) ?>
</div>