<article <?php (post_class('post-item')); ?>>
 
    <div class="row">
    <?php if( has_post_thumbnail() ): ?>
    <div class="col-lg-4 ">
    <a href="<?php echo e(get_permalink()); ?>" class="hs-featured-image-link" style="background-image:url( <?php echo e(the_post_thumbnail_url('full')); ?>">

      </a>
    </div>
    <?php endif; ?>
      <div class=" <?php if( !has_post_thumbnail()): ?> col-lg-12 <?php else: ?> col-lg-8 <?php endif; ?> d-flex align-items-center flex-wrap px-lg-5 py-lg-3">
            <div class="entry-summary">
                <header>
                    
                    <h2 class="entry-title"><a href="<?php echo e(get_permalink()); ?>"><?php echo e(get_the_title()); ?></a></h2>
                    <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  </header>
              <?php (the_excerpt()); ?>
              
              
            </div>
      </div>
    </div>
</article>