
<section class="single-content product-content" id="product-<?php the_ID(); ?>"> 
  
      <article <?php post_class() ?>>
          <div class="single-product">

           <div class="container woocommerce px-lg-0">
              <?php 
          do_action( 'woocommerce_before_single_product' );
          if ( post_password_required() ) {
            echo get_the_password_form(); // WPCS: XSS ok.
            return;
          }
?></div>
</div>


<div class="container woocommerce px-lg-0">
<div class="summary entry-summary">
             <?php do_action( 'woocommerce_single_product_summary' )?>
            </div>
          </div>
        </article>
</section>
<section class="gallery-content p-100 pb-0 wow">
  
    <div class="container woocommerce px-lg-0">
          <div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
        <?php do_action( 'woocommerce_before_single_product_summary' )?>


        
  </div>
</div>

</section>

<section class="description wow">
    <div class="container woocommerce px-lg-0">
          <div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
  ?>

</div>
</div>
</section>
          <div class="entry-content">

            

      
<?php do_action( 'woocommerce_after_single_product' ); ?>

</div>

        

          </div>
        
         
 <?php echo $__env->make('partials.tour-packages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        

<?php //comments_template('/partials/comments.blade.php') ?>