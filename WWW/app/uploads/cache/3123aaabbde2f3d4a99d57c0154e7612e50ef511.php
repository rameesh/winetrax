<?php $__env->startSection('content'); ?>
    <?php do_action('woocommerce_before_main_content') ?>
    <?php do_action('woocommerce_archive_description') ?>
    <?php if(have_posts()): ?>
        <?php do_action('woocommerce_before_shop_loop') ?>
        <?php if(wc_get_loop_prop('total')): ?>
            <div class="sw-product-archive row">
                <?php while(have_posts()): ?> <?php the_post() ?>
                <?php do_action('woocommerce_shop_loop') ?>
                <?php echo $__env->make('woocommerce.content-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <?php do_action( 'woocommerce_after_shop_loop' ) ?>
    <?php else: ?>
        <?php do_action( 'woocommerce_no_products_found' ) ?>
    <?php endif; ?>
    <?php do_action( 'woocommerce_after_main_content' ) ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>