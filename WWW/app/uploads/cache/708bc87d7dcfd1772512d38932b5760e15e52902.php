<?php $__env->startSection('content'); ?>
  <?php echo $__env->make('partials.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <section class="blog product-content">
      <div class="container-fluid p-100">
        <div class="container d-flex flex-wrap">
            <?php if(!have_posts()): ?>
            <div class="alert alert-warning">
              <?php echo e(__('Sorry, no results were found.', 'sage')); ?>

            </div>
            <?php echo get_search_form(false); ?>

          <?php endif; ?>
        
          <?php while(have_posts()): ?> <?php the_post() ?>
            <?php echo $__env->make('partials.content-'.get_post_type(), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endwhile; ?>
        </div>
      </div>
  </section>
  
  <div class="container d-flex justify-content-center align-items-center">
      <?php
      $args = array(
	'prev_text'          => '<i class="fa fa-angle-right"></i>',
	'next_text'          => '<i class="fa fa-angle-left"></i>',

);
the_posts_navigation($args);
  ?>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>