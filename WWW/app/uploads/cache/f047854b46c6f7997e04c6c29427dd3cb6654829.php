<div class="container px-7 py-4">
</div>
<?php if(get_the_post_thumbnail_url()  || is_home() || is_404() || is_page(array('my-account','cart','checkout'))): ?>
<div class="page-header container  <?php if(is_single()): ?> pt-5 pb-100 <?php else: ?> py-5 <?php endif; ?> <?php if(is_page(array('my-account','cart','checkout')) || is_account_page()): ?> woocommerce-title <?php else: ?> text-center px-7 <?php endif; ?>">
    
    <?php if(is_account_page()): ?>
    <h6 class="my-0 mx-4"><?php echo App::title(); ?></h6>

    <?php elseif(is_post_type_archive('product')): ?>

    <h1 class="m-0">Experiences</h1>

    <?php else: ?>

    <h1 class="m-0"><?php echo App::title(); ?></h1>

    <?php endif; ?>

    <?php if(is_single()): ?>
    <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

   
</div>
<?php endif; ?>

