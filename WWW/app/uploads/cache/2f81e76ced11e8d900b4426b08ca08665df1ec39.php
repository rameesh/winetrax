<section class="availability wows d-flex flex-wrap justify-content-center align-items-between">
    <div class="overlay">
        
    </div>

    <div class="container px-7 text-center d-flex justify-content-center">
            <div class="dropdown align-self-center">

                <h3>Book your premium wine experience today</h3>
				 
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                        Check Availability
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                         <a class="dropdown-item disabled" href="#"> Select Packege</a> 
                <?php
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => -1
                        );
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ) {
                        while ( $loop->have_posts() ) : $loop->the_post();?>
                        <a class="dropdown-item" href="<?php echo e(get_permalink()); ?>"><?php echo e(the_post_thumbnail('shop_thumbnail', ['class' => 'img-responsive mr-3', 'title' => 'Package'])); ?> <?php echo e(the_title()); ?></a> 
                        <?php endwhile;
                    } else {
                        echo __( 'No products found' );
                    }
                    wp_reset_postdata();
                ?>
                    </div>
                </div>
    </div>

</section>
<!--/.products-->