<header class="banner">
  <div class="container header-container d-flex">
    <nav class="navbar navbar-expand-lg static-top col-md-12 my-auto">
        <a class="brand" href="<?php echo e(home_url('/')); ?>"></a>
        
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
              data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
              aria-label="Toggle navigation">
        
        <i class="fas fa-bars"></i>
      </button>
    
    
      <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">

 
 
        <?php if(has_nav_menu('primary_navigation')): ?>
          <?php echo wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav nav justify-content-end']); ?>

        <?php endif; ?>

       <div class="social"> 
          <a href=" <?php echo e(the_field ('instagram', 'option')); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
          <a href=" <?php echo e(the_field ('tripadvisor', 'option')); ?>" target="_blank"><i class="fa fa-tripadvisor"></i></a>
          <a href="<?php echo e(the_field ('twitter', 'option')); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
          <a href="<?php echo e(the_field ('facebook', 'option')); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
      </div>
    </nav>
 </div>
</header>
<?php if(get_field('slides')): ?>

<section class="main-slider w-100" id="main-slider">

  <?php while(have_rows('slides') ): the_row(); 
  $description= get_sub_field('description'); 
  $image= get_sub_field('image');
  
?>

  <div class="slide d-flex align-items-center" style="background-image:url(<?php echo e($image['url']); ?>)">

    <div class="container text-center">
      <?php echo $description; ?>

    </div>

  </div>
  <?php endwhile 
?>
</section>

<?php elseif(is_home()): ?>

<?php
    $image = get_field('blog_banner', 'option');
    $size = 'full'; // (thumbnail, medium, large, full or custom size)

?>
<section class="archive-banner p-100 d-flex align-items-center" style="background-image:url(<?php echo e($image['url']); ?>) ">
    <?php if(!$image): ?>
    <div class="container px-7">
        <h1 class="m-0"><?php echo App::title(); ?></h1>
  </div>
  <?php endif; ?>
  </section>

  <?php elseif(is_single()): ?>

    <?php else: ?>
  <section class="featured-banner p-100 d-flex align-items-center" style="background-image:url( <?php echo e(the_post_thumbnail_url('full')); ?>) ">
      <?php if(!get_the_post_thumbnail_url()): ?>
      <div class="container px-7">
          <h1 class="m-0"><?php echo App::title(); ?></h1>
    </div>
    <?php endif; ?>
    </section>
<?php endif; ?>