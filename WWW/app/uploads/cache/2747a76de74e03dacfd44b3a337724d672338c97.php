<?php the_content() ?>

<?php if(is_front_page()): ?>
<?php echo $__env->make('partials.availability', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('partials.tour-packages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<?php endif; ?>
<?php if(is_page('contact')): ?>
<section class="contact wow p-100" id="contact">
        <div class="container p-100">
                
  
            <div class="row">

                <div class="col-lg-5">

                </div>

                <div class="col-lg-7">
                        <h3>Request information</h3>
                </div>
             
              <div class="col-lg-4 offset-lg-1 inquiries text-left">
                    <?php echo e(the_field ('contact', 'option')); ?>

                  
  
              </div>
  
              <div class="col-lg-7 text-left">
                
                 
                
                <?php
                   echo do_shortcode ('[contact-form-7 id="12" title="Contact form 1"]') 
                ?>
              </div>  
            </div>
          </div>
     </section>
<?php endif; ?>
<?php echo wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

