<section class="container page-banner mx-auto pb-100 pt-5 wow" style="background-image:url( <?php echo e(the_post_thumbnail_url('full')); ?>) ">
  
  </section>

<section class="p-100 single-content wow"> 
  <div class="container px-lg-0">
      <article <?php post_class() ?>>
          
          <div class="entry-content">
            <?php the_content() ?>
          </div>
          <footer>
            <?php echo wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

          </footer>
          
        </article>
  </div>
</section>
<?php //comments_template('/partials/comments.blade.php') ?>