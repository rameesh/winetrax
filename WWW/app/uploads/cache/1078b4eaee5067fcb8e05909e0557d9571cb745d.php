<?php $__env->startSection('content'); ?>
    <?php do_action( 'woocommerce_before_main_content' ) ?>
    <?php while(have_posts()): ?> <?php the_post() ?>
    <?php echo $__env->make('woocommerce.content-single-product', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endwhile; ?>
	<?php do_action( 'woocommerce_after_main_content' ) ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>