 <div class="col-xl-4 col-lg-6 d-flex align-items-top flex-wrap px-lg-3 wow">
      <article <?php wc_product_class( '', $product ); ?>>

            <div class="image-container">
                    <a href="<?php echo e(get_permalink()); ?>" class="hs-featured-image-link" style="background-image:url( <?php echo e(the_post_thumbnail_url('large')); ?>"></a>
            </div>

        <header class="px-5 pt-5"><h5><?php echo e(the_title()); ?></h5></header>

        
            <div class="entry-summary px-5 pb-5">
                    <?php 
                    the_excerpt(); ?>

                    <?php
                    do_action( 'woocommerce_before_shop_loop_item' );
                    do_action( 'woocommerce_after_shop_loop_item_title' );
                    do_action( 'woocommerce_after_shop_loop_item' );
                    ?>
            </div>
                  
               
              
        </article>
      </div>
