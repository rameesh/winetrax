<div class="container px-7 py-4">
</div>
<?php if(get_the_post_thumbnail_url()  || is_home()): ?>
<div class="page-header container  <?php if(is_single()): ?> pt-5 pb-100 <?php else: ?> py-5 <?php endif; ?> text-center px-7">
    <h1 class="m-0"><?php echo App::title(); ?></h1>

    <?php if(is_single()): ?>
    <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

   
</div>
<?php endif; ?>

