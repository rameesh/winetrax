@php the_content() @endphp

@if (is_front_page())
@include('partials.availability')
@include('partials.tour-packages')

{{-- <section class="contact wow p-100" id="contact">
  <div class="container p-100">
{{get_product_search_form()}}
</div>
</section>                --}}
@endif
@if (is_page('contact'))
<section class="contact wow p-100" id="contact">
        <div class="container p-100">
                
  
            <div class="row">

                <div class="col-lg-5">

                </div>

                <div class="col-lg-7">
                        <h3>Request information</h3>
                </div>
             
              <div class="col-lg-4 offset-lg-1 inquiries text-left">
                    {{ the_field ('contact', 'option')}}
                  
  
              </div>
  
              <div class="col-lg-7 text-left">
                
                 
                
                @php
                   echo do_shortcode ('[contact-form-7 id="12" title="Contact form 1"]') 
                @endphp
              </div>  
            </div>
          </div>
     </section>
@endif
{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
