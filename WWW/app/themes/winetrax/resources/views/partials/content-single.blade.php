@if (get_the_post_thumbnail_url())

<section class="container page-banner mx-auto pb-100 pt-5 wow" style="background-image:url( {{ the_post_thumbnail_url('full')}}) ">
  {{-- <img src="{{ the_post_thumbnail_url('full')}}" alt="" class="w-100"> --}}
  </section>
@endif
<section class="p-100 single-content wow"> 
  <div class="container px-lg-0">
      <article @php post_class() @endphp>
          
          <div class="entry-content">
            @php the_content() @endphp
          </div>
          <footer>
            {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
          </footer>
          
        </article>
  </div>
</section>
@php //comments_template('/partials/comments.blade.php') @endphp