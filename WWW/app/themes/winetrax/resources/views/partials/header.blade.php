<header class="banner">
  <div class="container header-container d-flex">
    <nav class="navbar navbar-expand-lg static-top col-md-12 my-auto">
        <a class="brand" href="{{ home_url('/') }}"></a>
        <div class="user d-block d-lg-none">@php 
        
            do_action( 'your_theme_header_top' )
           
            @endphp
    
            <a href=" /my-account/" target="_blank" class="my-account float-right"><i class="fa fa-user"></i></a> </div>
      

      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
              data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
              aria-label="Toggle navigation">
        {{-- <span class="navbar-toggler-icon"></span> --}}
        <i class="fa fa-bars"></i>
      </button>
    
    
      <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">

 
 
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav nav justify-content-end']) !!}
        @endif
        <div class="user d-none d-lg-block">@php 
        
            do_action( 'your_theme_header_top' )
           
            @endphp
    
            <a href=" /my-account/" target="_blank" class="my-account"><i class="fa fa-user"></i></a> </div>
      
       <div class="social"> 
          
          <a href=" {{ the_field ('instagram', 'option')}}" target="_blank"><i class="fa fa-instagram"></i></a>
          {{-- <a href=" {{ the_field ('tripadvisor', 'option')}}" target="_blank"><i class="fa fa-tripadvisor"></i></a> --}}
          <a href="{{ the_field ('linkedin', 'option')}}" target="_blank"><i class="fa fa-linkedin"></i></a>
          <a href="{{ the_field ('facebook', 'option')}}" target="_blank"><i class="fa fa-facebook"></i></a>
      </div>
    </nav>
 </div>
</header>
@if (get_field('slides'))

<section class="main-slider w-100" id="main-slider">

  @php while(have_rows('slides') ): the_row(); 
  $description= get_sub_field('description'); 
  $more= get_sub_field('more_text'); 
  $image= get_sub_field('image');
  
@endphp

  <div class="slide d-flex align-items-center flex-wrap" style="background-image:url({{ $image['url'] }})">

    <div class="container text-center">
      {!! $description !!}
    </div>

    <div class="text">
     <div class="col-md-6 mx-auto text-center">
      <p> {!! $more !!}</p>
     </div>
    </div>

  </div>
  @php endwhile 
@endphp
</section>

@elseif (is_home())

@php
    $image = get_field('blog_banner', 'option');
    $size = 'full'; // (thumbnail, medium, large, full or custom size)

@endphp
<section class="archive-banner p-100 d-flex align-items-center" style="background-image:url({{ $image['url'] }}) ">
    @if (!$image)
    <div class="container px-7">
        <h1 class="m-0">{!! App::title() !!}</h1>
  </div>
  @endif

  </section>

  @elseif (is_single() || is_404() || is_page(array('my-account','cart', 'checkout','regions')) || is_post_type_archive('product'))

    @else
  <section class="featured-banner p-100 d-flex align-items-center" style="background-image:url( {{ the_post_thumbnail_url('full')}}) ">
      @if (!get_the_post_thumbnail_url())
      <div class="container px-7">
          <h1 class="m-0">{!! App::title() !!}</h1>
    </div>
    @endif
    </section>
@endif

@if (is_front_page())
<a class="arrow" href="#wrap">
</a>
@endif