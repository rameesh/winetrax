<article @php(post_class('post-item'))>
 
    <div class="row">
    @if ( has_post_thumbnail() )
    <div class="col-lg-4 ">
    <a href="{{ get_permalink() }}" class="hs-featured-image-link" style="background-image:url( {{ the_post_thumbnail_url('full')}}">

      </a>
    </div>
    @endif
      <div class=" @if ( !has_post_thumbnail()) col-lg-12 @else col-lg-8 @endif d-flex align-items-center flex-wrap px-lg-5 py-lg-3">
            <div class="entry-summary">
                <header>
                    
                    <h2 class="entry-title"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
                    @include('partials/entry-meta')
                  </header>
              @php(the_excerpt())
              {{-- <a class="more-link" href="{{ get_permalink() }}">Read More ></a> --}}
              
            </div>
      </div>
    </div>
</article>