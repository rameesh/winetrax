<div class="container px-7 py-4">
</div>
@if (get_the_post_thumbnail_url()  || is_home() || is_404() || is_page(array('my-account','cart','checkout')))
<div class="page-header container  @if (is_single()) pt-5 pb-100 @else py-5 @endif @if (is_page(array('my-account','cart','checkout')) || is_account_page()) woocommerce-title @else text-center px-7 @endif">
    
    @if (is_account_page())
    <h6 class="my-0 mx-4">{!! App::title() !!}</h6>

    @elseif (is_post_type_archive('product'))

    <h1 class="m-0">Experiences</h1>

    @else

    <h1 class="m-0">{!! App::title() !!}</h1>

    @endif

    @if (is_single())
    @include('partials/entry-meta')
    @endif

   
</div>
@endif

