@php 

$custom_query_args = array( 

            'post_type'      => 'product',
            'posts_per_page' => -1,
            'orderby'   => 'id',
            'order' => 'asc', // Randomize the results
            'operator' => 'IN',
            'post_status' => 'publish',
        
      );
        // Initiate the custom query
        $custom_query = new WP_Query( $custom_query_args );
        
        if ( $custom_query->have_posts() ):
        $price = get_post_meta( get_the_ID(), '_regular_price', true);
        @endphp

<section class="@if (is_singular('product'))more-packages @else p-100 pb-0 @endif tour-packages wow">
        <div class="container">
            <h3>Tour Packages</h3>
        </div>
        
        <div class="container d-flex pt-5 pb-100 flex-wrap" id="tour-packages">

            @php 
            while ( $custom_query->have_posts() ) : $custom_query->the_post();
            
      @endphp

            <!-- do stuff -->
          
          
                <div class="tour-box px-1 mb-5">
                        {{-- <div class="img-container w-100 mb-5">
                                <a href="{{ get_permalink() }}" class="case-study-link d-flex justify-content-center align-items-center" style="background-image: url('{{ the_post_thumbnail_url('full')}}');">
                                    <div class="overlay mx-1"></div>
                                    <div class="text text-center w-100">
                                    <h5>{{ the_title()}}</h5>
                                    </div>
                                </a>
                            </div>
            
                        <p class="text-center price">{!! $price !!}</p>
                                <p class="text-center"><a href="{{ get_permalink() }}" class="btn">Choose</a></p> --}}

                                @php  do_action( 'woocommerce_shop_loop' );
                                wc_get_template_part( 'content', 'product' );@endphp
                                
                </div>
                @endwhile

  
      @php (wp_reset_postdata()) 
          </div>
        </section>
        @endif

        