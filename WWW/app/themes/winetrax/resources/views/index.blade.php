@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  <section class="blog p-100">
      <div class="container">
        <div class="row">
            @if (!have_posts())
            <div class="alert alert-warning">
              {{ __('Sorry, no results were found.', 'sage') }}
            </div>
            {!! get_search_form(false) !!}
          @endif
        
          @while (have_posts()) @php the_post() @endphp
            @include('partials.content-'.get_post_type())
          @endwhile
        </div>
      </div>
  </section>
  {{-- {!! get_the_posts_navigation() !!} --}}

  <div class="container d-flex justify-content-end align-items-center">
      @php
      $args = array(
	'prev_text'          => '<i class="fa fa-angle-right"></i>',
	'next_text'          => '<i class="fa fa-angle-left"></i>',

);
the_posts_navigation($args);
  @endphp
  </div>
@endsection
