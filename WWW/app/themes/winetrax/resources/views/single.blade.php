@extends('layouts.app')

@section('content')

@if (!is_product())
@include('partials.page-header')
@endif

  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-single-'.get_post_type())
  @endwhile
@endsection
