import {WOW} from 'wowjs/dist/wow.min.js';

export default {
  init() {
    // JavaScript to be fired on all pages
    // wow.js
    $('.wp-block-mkl-section-block').addClass('wow fadeInUp');
    $('.wow').addClass('fadeInUp');



    // Animate css
    // $('.btn').addClass('animated infinite pulse delay-2s');
   
    $('.dropdown-item').addClass('animated fadeInDown');
    $('.woocommerce-notices-wrapper').addClass('animated infinite bounce delay-2s');
    $('.arrow').addClass('animated bounceInDown delay-2s');
    $('.dropdown-item').attr({'data-wow-delay': '.10s'});

    // Scroll
 $('.arrow').click(function() {
  // console.log('click');

  
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    // console.log(target);
    if (target.length) {
      // console.log(target+'exists');
      $('html, body').animate({
        scrollTop: target.offset().top - 0,
      }, 600);
      return false;
    }
  }
}); 


// ekko-Lightbox
/* eslint-disable */
  /**
  * Prevent default click behavior on WordPress gallery image links
  */
  $(document).on('click', '.blocks-gallery-item a', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox({
      showArrows: true,
      alwaysShowClose: true,
      title: '',
      footer: '',
      maxWidth: 1280,
      maxHeight: 900,
      
      strings: {
     
          close: 'Close',
      
          fail: 'Failed to load image:',
      
          type: 'Could not detect remote target type. Force the type using data-type',

        },

    });
  });

  // Make an ekko lightbox out of WordPress galleries
  $('.blocks-gallery-item a[href]')

  // Test whether this link is directly to an image file
  .filter(function() {
    return /(jpg|gif|png)$/.test($(this).attr('href'));
  }).each(function() {

  // Get link attr
  var link = $(this).attr('href');

  // Get the image ALT text
  var alt = $(this).find('img').attr('alt');

  // var figcaption = $('.blocks-gallery-item').find('figure').attr('figcaption'); 

  // Strip the file extension
  var filename = link.substr(0, $(this).attr('href').lastIndexOf('.')) || $(this).attr('href');

  // Get the index of just the filename without the path
  var fileNameIndex = filename.lastIndexOf("/") + 1;

  // Return just the filename without the path and without the extension
  filename = filename.substr(fileNameIndex);

  // Get the ALT text so we can use it as a caption ONLY if it's NOT the same as the filename!
  // WordPress automatically uses the filename as ALT text if no caption or ALT is set, so we don't want to use it if they're the same.
  var caption = (filename === alt) ? '' : alt;

  // Get the gallery ID
  var gallery = $(this).closest('.blocks-gallery-item').attr('class');

  // Add ekko lightbox data-attributes
  $(this).attr({'data-toggle': 'lightbox', 'data-gallery': gallery, 'data-footer': caption});

  

  });
    


/* eslint-disable */

const wow = new WOW(
  {
  boxClass:     'wow',      // default
  animateClass: 'animated', // default
  offset:       0,          // default
  mobile:       true,       // default
  live:         true,        // default
  scrollContainer: null,    // optional scroll container selector, otherwise use window,
  resetAnimation: true,     // reset animation on end (default is true)
}
);
wow.init();




     // Slider
     $('.main-slider').slick({
      dots: true,
      infinite: true,
      speed: 400,
      cssEase: 'linear',
      autoplay: true,
      autoplaySpeed: 8000,
      fade: true,
      arrows: false,
      // centerMode: true,
      draggable: false,
    });

    // tour-packages
  /* eslint-disable */
    $('#tour-packages').slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
          }
        },
        {
          breakpoint: 980,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
      
    });
  /* eslint-enable */
    

    // Instafeed
     /* eslint-disable */
     var Instafeed = require("instafeed.js");

     var galleryFeed = new Instafeed({
      get: "user",
      userId:9476680114,
      accessToken:'9476680114.1677ed0.6a39bd66ec4c4b8ca58360a2626a376b',
      resolution: "standard_resolution",
      sortBy: 'random',
      limit: 6,
      template: '<div class="parent"><a href="{{link}}" target="_blank"><div class="img-featured-container" style="background-image:url({{image}})"><div class="img-backdrop"></div></div></a></div>',
      target: "instafeed-gallery-feed",
      after: function() {
        // disable button if no more results to load
        if (!this.hasNext()) {
          btnInstafeedLoad.setAttribute('disabled', 'disabled');
        }
      },
    });
    galleryFeed.run();
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
