<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Bookings WC ajax callbacks.
 */
class WC_Bookings_WC_Ajax {

	/**
	 * Constructor.
	 */
	public function __construct() {
		add_action( 'wc_ajax_wc_bookings_find_booking_slots', array( $this, 'find_booking_slots' ) );
		add_action( 'wc_ajax_wc_bookings_find_booked_day_blocks', array( $this, 'find_booked_day_blocks' ) );
		add_action( 'wc_ajax_wc_bookings_get_all_bookable_products', array( $this, 'get_all_bookable_products' ) );
		add_action( 'wc_ajax_wc_bookings_get_all_categories_with_bookable_products', array( $this, 'get_all_categories_with_bookable_products' ) );
		add_action( 'wc_ajax_wc_bookings_get_all_resources', array( $this, 'get_all_resources' ) );
	}

	/**
	 * Find booking slots for:
	 * - A list of products
	 * - For a specific date range
	 * - Filtered by categories
	 * - Filtered by resources
	 */
	public function find_booking_slots() {
		check_ajax_referer( 'find-booking-slots', 'security' );

		$product_ids  = isset( $_GET['product'] ) ? array_map( 'absint', explode( ',', $_GET['product_ids'] ) ) : array();
		$category_ids = isset( $_GET['category'] ) ? array_map( 'absint', explode( ',', $_GET['category_ids'] ) ) : array();

		$min_date     = isset( $_GET['min_date'] ) ? strtotime( $_GET['min_date'] ) : 0;
		$max_date     = isset( $_GET['max_date'] ) ? strtotime( $_GET['max_date'] ) : 0;

		$resource_ids = isset( $_GET['resource'] ) ? array_map( 'absint', explode( ',', $_GET['resource_ids'] ) ) : array();

		$timezone_offset = isset( $_GET['timezone_offset'] ) ? absint( $_GET['timezone_offset'] ) : 0;

		// If no product ids are specified, just use all products.
		if ( empty( $product_ids ) ) {
			$product_ids = WC_Data_Store::load( 'product-booking' )->get_bookable_product_ids();
		}

		// If category ids are specified filter the product ids.
		if ( ! empty( $category_ids ) ) {
			$product_ids = array_filter( $product_ids, function( $product_id ) use ( $category_ids ) {
				return array_reduce( $category_ids, function( $is_in_category, $category_id ) use ( $product_id ) {
					$term = get_term_by( 'id', $category_id, 'product_cat' );

					if ( ! $term ) {
						return $is_in_category;
					}

					return $is_in_category || has_term( $term, 'product_cat', $product_id );
				}, false );
			} );
		}

		// Calculate partially booked/fully booked/unavailable days for each product.
		$booked_data = array_map( function( $product_id ) use ( $min_date, $max_date, $timezone_offset, $resource_ids ) {
			$booked = WC_Bookings_Controller::find_booked_day_blocks(
				$product_id,
				$min_date,
				$max_date,
				'Y-n-j',
				$timezone_offset,
				$resource_ids
			);

			$booked['product_id']  = $product_id;

			return $booked;
		}, $product_ids );

		wp_send_json( $booked_data );
	}

	/**
	 * This endpoint is supposed to replace the back-end logic in booking-form.
	 */
	public function find_booked_day_blocks() {

		check_ajax_referer( 'find-booked-day-blocks', 'security' );

		$product_id = absint( $_GET['product_id'] );

		if ( empty( $product_id ) ) {
			wp_send_json_error( 'Missing product ID' );
			exit;
		}

		try {

			$args                          = array();
			$product                       = new WC_Product_Booking( $product_id );
			$args['availability_rules']    = array();
			$args['availability_rules'][0] = $product->get_availability_rules();
			$args['min_date']              = isset( $_GET['min_date'] ) ? strtotime( $_GET['min_date'] ) : $product->get_min_date();
			$args['max_date']              = isset( $_GET['max_date'] ) ? strtotime( $_GET['max_date'] ) : $product->get_max_date();

			$min_date        = ( ! isset( $_GET['min_date'] ) ) ? strtotime( "+{$args['min_date']['value']} {$args['min_date']['unit']}", current_time( 'timestamp' ) ) : $args['min_date'];
			$max_date        = ( ! isset( $_GET['max_date'] ) ) ? strtotime( "+{$args['max_date']['value']} {$args['max_date']['unit']}", current_time( 'timestamp' ) ) : $args['max_date'];
			$timezone_offset = isset( $_GET['timezone_offset'] ) ? $_GET['timezone_offset'] : 0;

			if ( $product->has_resources() ) {
				foreach ( $product->get_resources() as $resource ) {
					$args['availability_rules'][ $resource->ID ] = $product->get_availability_rules( $resource->ID );
				}
			}

			$booked = WC_Bookings_Controller::find_booked_day_blocks( $product_id, $min_date, $max_date, 'Y-n-j', $timezone_offset );

			$args['partially_booked_days'] = $booked['partially_booked_days'];
			$args['fully_booked_days']     = $booked['fully_booked_days'];
			$args['unavailable_days']      = $booked['unavailable_days'];
			$args['restricted_days']       = $product->has_restricted_days() ? $product->get_restricted_days() : false;

			$buffer_days = array();
			if ( ! in_array( $product->get_duration_unit(), array( 'minute', 'hour' ) ) ) {
				$buffer_days = WC_Bookings_Controller::get_buffer_day_blocks_for_booked_days( $product, $args['fully_booked_days'] );
			}

			$args['buffer_days']           = $buffer_days;

			wp_send_json( $args );

		} catch ( Exception $e ) {

			wp_die();

		}
	}

	/**
	 * Gets all the bookable products.
	 *
	 * @since 1.13.0
	 * @return JSON $payload
	 */
	public function get_all_bookable_products() {
		check_ajax_referer( 'get-all-bookable-products', 'security' );

		try {
			$args = apply_filters( 'get_booking_products_args', array(
				'post_status'      => 'publish',
				'post_type'        => 'product',
				'posts_per_page'   => -1,
				'tax_query'        => array(
					array(
						'taxonomy' => 'product_type',
						'field'    => 'slug',
						'terms'    => 'booking',
					),
				),
				'suppress_filters' => true,
			) );

			$payload = new WP_Query( $args );

			wp_send_json( $payload->posts );
		} catch ( Exception $e ) {
			wp_die();
		}
	}

	/**
	 * Gets all categories that contain bookable products.
	 *
	 * @return JSON $payload
	 */
	public function get_all_categories_with_bookable_products() {
		check_ajax_referer( 'get-all-categories-with-bookable-products', 'security' );

		try {
			$categories         = array();
			$product_categories = get_terms( 'product_cat' );

			if ( ! is_array( $product_categories ) ) {
				wp_send_json( $categories );
			}

			foreach ( $product_categories as $product_category ) {
				$args = apply_filters( 'get_categories_booking_products_args', array(
					'posts_per_page' => -1,
					'post_type'      => 'product',
					'tax_query'      => array(
						'relation'     => 'AND',
						array(
							'taxonomy' => 'product_cat',
							'field'    => 'slug',
							'terms'    => $product_category->slug,
						),
						array(
							'taxonomy' => 'product_type',
							'field'    => 'slug',
							'terms'    => 'booking',
						),
					),
				), $product_category );

				$products = new WP_Query( $args );

				if ( $products->have_posts() ) {
					$categories[] = $product_category->name;
				}
			}

			wp_send_json( $categories );
		} catch ( Exception $e ) {
			wp_die();
		}
	}

	/**
	 * Gets all resources.
	 *
	 * @since 1.13.0
	 * @return JSON $payload
	 */
	public function get_all_resources() {
		check_ajax_referer( 'get-all-resources', 'security' );

		try {
			$args = apply_filters( 'get_all_resources_args', array(
				'post_status'      => 'publish',
				'post_type'        => 'bookable_resource',
				'posts_per_page'   => -1,
				'suppress_filters' => true,
			) );

			$payload = get_posts( $args );

			wp_send_json( $payload->posts );
		} catch ( Exception $e ) {
			wp_die();
		}
	}
}

new WC_Bookings_WC_Ajax();
