<?php
/**
 * Plugin Name: WooCommerce Bookings
 * Plugin URI: https://woocommerce.com/products/woocommerce-bookings/
 * Description: Setup bookable products such as for reservations, services and hires.
 * Version: 1.13.3
 * Author: Automattic
 * Author URI: https://woocommerce.com
 * Text Domain: woocommerce-bookings
 * Domain Path: /languages
 * Tested up to: 5.0
 * WC tested up to: 3.5
 * WC requires at least: 2.6
 *
 * Copyright: © 2019 WooCommerce.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * Woo: 390890:911c438934af094c2b38d5560b9f50f3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// phpcs:disable WordPress.Files.FileName

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once 'woo-includes/woo-functions.php';
}

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '911c438934af094c2b38d5560b9f50f3', '390890' );

/**
 * WooCommerce fallback notice.
 *
 * @since 1.13.0
 */
function woocommerce_bookings_missing_wc_notice() {
	/* translators: %s WC download URL link. */
	echo '<div class="error"><p><strong>' . sprintf( esc_html__( 'Bookings requires WooCommerce to be installed and active. You can download %s here.', 'woocommerce-bookings' ), '<a href="https://woocommerce.com/" target="_blank">WooCommerce</a>' ) . '</strong></p></div>';
}

if ( ! defined( 'WC_BOOKINGS_ABSPATH' ) ) {
	define( 'WC_BOOKINGS_ABSPATH', dirname( __FILE__ ) . '/' );
}

require_once WC_BOOKINGS_ABSPATH . 'includes/lib/action-scheduler/action-scheduler.php';

register_activation_hook( __FILE__, 'woocommerce_bookings_activate' );

function woocommerce_bookings_activate() {
	if ( ! class_exists( 'WooCommerce' ) ) {
		add_action( 'admin_notices', 'woocommerce_bookings_missing_wc_notice' );
		return;
	}

	if ( is_admin() ) {
		require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-inbox-notices.php';
	}

	if ( class_exists( 'WC_Admin_Notes' ) ) {
		WC_Bookings_Inbox_Notice::add_activity_panel_inbox_welcome_note();
	} else {
		$notice_html = '<strong>' . esc_html__( 'Bookings has been activated!', 'woocommerce-bookings' ) . '</strong><br><br>';
		/* translators: 1: href link to list of bookings */
		$notice_html .= sprintf( __( '<a href="%s">Add or edit a product</a> to manage bookings in the Product Data section for individual products and then go to the <a href="%s" target="_blank">Bookings page</a> to manage them individually.', 'woocommerce-bookings' ), admin_url( 'post-new.php?post_type=product&bookable_product=1' ), admin_url( 'edit.php?post_type=wc_booking' ) );

		WC_Admin_Notices::add_custom_notice( 'woocommerce_bookings_activation', $notice_html );
	}

	// Register the rewrite endpoint before permalinks are flushed.
	add_rewrite_endpoint( apply_filters( 'woocommerce_bookings_account_endpoint', 'bookings' ), EP_PAGES );

	// Flush Permalinks.
	flush_rewrite_rules();
}


if ( ! class_exists( 'WC_Bookings' ) ) :
	define( 'WC_BOOKINGS_VERSION', '1.13.3' );
	define( 'WC_BOOKINGS_DB_VERSION', '1.13.0' );
	define( 'WC_BOOKINGS_TEMPLATE_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' );
	define( 'WC_BOOKINGS_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
	define( 'WC_BOOKINGS_MAIN_FILE', __FILE__ );
	define( 'WC_BOOKINGS_GUTENBERG_EXISTS', function_exists( 'register_block_type' ) ? true : false );

	/**
	 * WC Bookings class
	 */
	class WC_Bookings {
		/**
		 * The single instance of the class.
		 *
		 * @var $_instance
		 * @since 1.13.0
		 */
		protected static $_instance = null;

		/**
		 * Constructor.
		 *
		 * @since 1.13.0
		 */
		public function __construct() {
			$this->includes();
			register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
			add_filter( 'plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 2 );

			// Do migrations.
			WC_Bookings_Install::init();
		}

		/**
		 * Show row meta on the plugin screen.
		 *
		 * @access public
		 * @param  mixed $links Plugin Row Meta
		 * @param  mixed $file  Plugin Base file
		 * @return array
		 */
		public function plugin_row_meta( $links, $file ) {
			if ( plugin_basename( WC_BOOKINGS_MAIN_FILE ) === $file ) {
				$row_meta = array(
					'docs'    => '<a href="' . esc_url( apply_filters( 'woocommerce_bookings_docs_url', 'https://docs.woocommerce.com/documentation/plugins/woocommerce/woocommerce-extensions/woocommerce-bookings/' ) ) . '" title="' . esc_attr( __( 'View Documentation', 'woocommerce-bookings' ) ) . '">' . __( 'Docs', 'woocommerce-bookings' ) . '</a>',
					'support' => '<a href="' . esc_url( apply_filters( 'woocommerce_bookings_support_url', 'https://woocommerce.com/my-account/tickets/' ) ) . '" title="' . esc_attr( __( 'Visit Premium Customer Support', 'woocommerce-bookings' ) ) . '">' . __( 'Premium Support', 'woocommerce-bookings' ) . '</a>',
				);

				return array_merge( $links, $row_meta );
			}

			return (array) $links;
		}

		/**
		 * Main Bookings Instance.
		 *
		 * Ensures only one instance of Bookings is loaded or can be loaded.
		 *
		 * @since 1.13.0
		 * @return WC_Bookings
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		}

		/**
		 * Cloning is forbidden.
		 *
		 * @since 1.13.0
		 */
		public function __clone() {
			wc_doing_it_wrong( __FUNCTION__, __( 'Cloning is forbidden.', 'woocommerce-bookings' ), WC_BOOKINGS_VERSION );
		}

		/**
		 * Unserializing instances of this class is forbidden.
		 *
		 * @since 1.13.0
		 */
		public function __wakeup() {
			wc_doing_it_wrong( __FUNCTION__, __( 'Unserializing instances of this class is forbidden.', 'woocommerce-bookings' ), WC_BOOKINGS_VERSION );
		}

		/**
		 * Cleanup on plugin deactivation.
		 *
		 * @since 1.11
		 */
		public function deactivate() {
			if ( class_exists( 'WC_Admin_Notes' ) ) {
				WC_Bookings_Inbox_Notice::remove_activity_panel_inbox_notes();
			} else {
				WC_Admin_Notices::remove_notice( 'woocommerce_bookings_activation' );
			}
		}

		/**
		 * Load Classes.
		 */
		public function includes() {
			/**
			 * Load 3.0.x classes and backwards compatibility code when using older versions of WooCommerce.
			 *
			 * @since 1.10.0
			 * @todo  remove this when 2.6.x support is dropped.
			 */
			if ( version_compare( WC_VERSION, '3.0', '<' ) ) {
				if ( ! class_exists( 'WC_Data_Store' ) ) {
					require_once WC_BOOKINGS_ABSPATH . 'includes/compatibility/class-wc-data-store.php';
				}
				if ( ! class_exists( 'WC_Data_Exception' ) ) {
					require_once WC_BOOKINGS_ABSPATH . 'includes/compatibility/class-wc-data-exception.php';
				}
				if ( ! class_exists( 'WC_Data_Store_WP' ) ) {
					require_once WC_BOOKINGS_ABSPATH . 'includes/compatibility/class-wc-data-store-wp.php';
				}
				if ( ! class_exists( 'WC_Product_Data_Store_CPT' ) ) {
					require_once WC_BOOKINGS_ABSPATH . 'includes/compatibility/class-wc-product-data-store-cpt.php';
				}
				if ( ! class_exists( 'WC_DateTime' ) ) {
					require_once WC_BOOKINGS_ABSPATH . 'includes/compatibility/class-wc-datetime.php';
				}
			}

			require_once WC_BOOKINGS_ABSPATH . 'includes/compatibility/class-wc-ajax-compat.php';

			if ( ! class_exists( 'WC_Bookings_Data' ) ) {
				require_once WC_BOOKINGS_ABSPATH . 'includes/compatibility/abstract-wc-bookings-data.php';
			}

			// Initialize.
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-bookings-init.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-bookings-timezone-settings.php';

			// Install.
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-bookings-install.php';

			// WC AJAX.
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-bookings-wc-ajax.php';

			// Objects.
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-objects/class-wc-product-booking.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-objects/class-wc-product-booking-resource.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-objects/class-wc-product-booking-person-type.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-objects/class-wc-booking.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-objects/class-wc-global-availability.php';

			// Stores.
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-stores/class-wc-booking-data-store.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-stores/class-wc-product-booking-data-store-cpt.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-stores/class-wc-product-booking-resource-data-store-cpt.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-stores/class-wc-product-booking-person-type-data-store-cpt.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/data-stores/class-wc-global-availability-data-store.php';

			require_once WC_BOOKINGS_ABSPATH . 'includes/wc-bookings-functions.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-booking-form-handler.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-booking-order-manager.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-product-booking-manager.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-bookings-controller.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-booking-cron-manager.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-bookings-ics-exporter.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/gateways/class-wc-bookings-gateway.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-bookings-google-calendar-connection.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/booking-form/class-wc-booking-form.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-booking-coupon.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-product-class-loader.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-product-booking-rule-manager.php';

			if ( class_exists( 'WC_Product_Addons' ) ) {
				require_once WC_BOOKINGS_ABSPATH . 'includes/integrations/class-wc-bookings-addons.php';
			}

			if ( class_exists( 'WC_Deposits' ) ) {
				require_once WC_BOOKINGS_ABSPATH . 'includes/integrations/class-wc-bookings-deposits.php';
			}

			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-booking-privacy.php';

			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-booking-email-manager.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-booking-cart-manager.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/class-wc-booking-checkout-manager.php';

			require_once WC_BOOKINGS_ABSPATH . 'includes/lib/php-rrule/RfcParser.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/lib/php-rrule/RRuleInterface.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/lib/php-rrule/RRule.php';
			require_once WC_BOOKINGS_ABSPATH . 'includes/lib/php-rrule/RSet.php';

			if ( is_admin() ) {
				require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-inbox-notices.php';
				require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-menus.php';
				require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-report-dashboard.php';
				require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-tools.php';
				require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-admin.php';
				require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-ajax.php';
				require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-addons.php';
				require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-products-export.php';
				require_once WC_BOOKINGS_ABSPATH . 'includes/admin/class-wc-bookings-products-import.php';
			}
		}
	}
endif;

add_action( 'plugins_loaded', 'woocommerce_bookings_init', 10 );

function woocommerce_bookings_init() {
	load_plugin_textdomain( 'woocommerce-bookings', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );

	if ( ! class_exists( 'WooCommerce' ) ) {
		add_action( 'admin_notices', 'woocommerce_bookings_missing_wc_notice' );
		return;
	}

	$GLOBALS['wc_bookings'] = WC_Bookings::instance();
}
