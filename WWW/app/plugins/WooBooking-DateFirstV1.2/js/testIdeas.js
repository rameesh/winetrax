var dates = new Array();

function addDate(date) {
    if (jQuery.inArray(date, dates) < 0) dates.push(date);
}

function removeDate(index) {
    dates.splice(index, 1);
}

function printArray() {
    var printArr = new String;
    dates.forEach(function (val) {
        printArr += '<h4>' + val + '</h4>';
    });
    $('#print-array').html(printArr);
}
// Adds a date if we don't have it yet, else remove it
function addOrRemoveDate(date) {
    var index = jQuery.inArray(date, dates);
    if (index >= 0) 
        removeDate(index);
    else 
        addDate(date);

    printArray();
}

// Takes a 1-digit number and inserts a zero before it
function padNumber(number) {
    var ret = new String(number);
    if (ret.length == 1) ret = "0" + ret;
    return ret;
}

$("#datepicker").datepicker({
    onSelect: function (dateText, inst) {
        addOrRemoveDate(dateText);
    },
    beforeShowDay: function (date) {
        var year = date.getFullYear();
        // months and days are inserted into the array in the form, e.g "01/01/2009", but here the format is "1/1/2009"
        var month = padNumber(date.getMonth() + 1);
        var day = padNumber(date.getDate());
        // This depends on the datepicker's date format
        var dateString = month + "/" + day + "/" + year;

        var gotDate = jQuery.inArray(dateString, dates);
        if (gotDate >= 0) {
            // Enable date so it can be deselected. Set style to be highlighted
            return [true, "ui-state-highlight"];
        }
        // Dates not in the array are left enabled, but with no extra style
        return [true, ""];
    }
});


function days() {
    var a = $("#datepicker_start").datepicker('getDate').getTime(),
        b = $("#datepicker_end").datepicker('getDate').getTime(),
        c = 24*60*60*1000,
        diffDays = Math.round(Math.abs((a - b)/(c)));
    console.log(diffDays); //show difference
}

<?php
	//WOO MODS
//good function, no longer useing
/*
function woo_template_replace( $located, $template_name, $args, $template_path, $default_path ) {
	global $post;
   $terms =  wp_get_object_terms( $post->ID,  'product_cat' );
   $categories = array();
	foreach ( $terms as $term ) $categories[] = $term->slug;
		if ( in_array( 'dis_wcb_datefirst', $categories ) ) {
		if( file_exists( plugin_dir_path(__FILE__) . 'woocommerce/' . $template_name ) ) {
		    $located = plugin_dir_path(__FILE__) . 'woocommerce/' . $template_name;
		}
		
		
	}
	return $located;
}


function woo_get_template_part( $template , $slug , $name ) {
   global $post;
   $terms =  wp_get_object_terms( $post->ID,  'product_cat' );
   $categories = array();
	foreach ( $terms as $term ) $categories[] = $term->slug;
		if ( in_array( 'dis_wcb_datefirst', $categories ) ) {
			if( empty( $name ) ) {
			    if( file_exists( plugin_dir_path(__FILE__) . "/woocommerce/{$slug}.php" ) ) {
			        $template = plugin_dir_path(__FILE__) . "/woocommerce/{$slug}.php";
			    }
			} else {
			    if( file_exists( plugin_dir_path(__FILE__) . "/woocommerce/{$slug}-{$name}.php" ) ) {
			        $template = plugin_dir_path(__FILE__) . "/woocommerce/{$slug}-{$name}.php";
			    }
			
			}
		
		
		}
		return $template;

}
add_filter( 'wc_get_template' , 'woo_template_replace' , 10 , 5 );

add_filter( 'wc_get_template_part' , 'woo_get_template_part' , 10 , 3 );