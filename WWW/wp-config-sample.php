<?php

define( 'WP_CONTENT_DIR', '/Users/swim/Sites/winetrax/WWW/app' ); // Do not remove. Removing this line could break your site. Added by Security > Settings > Change Content Directory.
define( 'WP_CONTENT_URL', 'http://dev.winetrax.com.au/app' ); // Do not remove. Removing this line could break your site. Added by Security > Settings > Change Content Directory.

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'winetrax' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'r2sRi0TyKXZfl5sMa5g5fBr[IX.Y<)tbaiNB9MfB>()u5) oet1h/m`dR-T}`]]a' );
define( 'SECURE_AUTH_KEY',  'Y24Pz%.MHi*QpD@#gz.6o9KHvg_%Y+s9iQUVpC*bAQh:vqp.=Z(G=E-OFt.?0q_O' );
define( 'LOGGED_IN_KEY',    'k[Y`pQde#~xY_9%ReiPTwW3[{LvREUL)N9wf8>4;w+ur-d{NQVF7n?j_,^qVBW p' );
define( 'NONCE_KEY',        '~Jct`b5jD;3b;=DC9tItV-K<;!kV<KUTB0&Q (Isv>Ag!F67G9^>6]_{y$Y%il9n' );
define( 'AUTH_SALT',        '$s&5.EPPTY$26  ^rpP^SD?zUa4#p,$T7WB2hsmXB-P/$~-.4DU<`]qeZT.9;y#-' );
define( 'SECURE_AUTH_SALT', 'Fj0Z*g)^vNJQ{72ba`3<`+}|]?y1#.K?v^.oCzBsWmulBdT] DbHxA*dMQ*L_T 7' );
define( 'LOGGED_IN_SALT',   ')Et1&Z*P8lkrpKLsVpMH&U.n{ajL|}@mR>nq0<|VIjZ||:hu|-L pe:_C]YgOCuf' );
define( 'NONCE_SALT',       '!tA5oN8XDCav ,g|HB:@)X*7ON`c2L|^X:UX3?`:b9afp}IBS*n9p2EG?_{[Z85Z' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'xglhxyb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
